import os

SQL_USER = 'imdbs_user'
SQL_PASSWORD = 'imdbs_password'
SQL_DATABASE = 'imdb_db'
SQL_CONNECTION_NAME = 'imdb_db-pm-1:europe-west6:imdb_db-pm-1'
 

class Config(object):
    SQLALCHEMY_ECHO =  True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    DEBUG = True
    TESTING = True


class DevelopmentConfig(Config):
    SQLALCHEMY_DATABASE_URI = (
        'mysql+pymysql://{nam}:{pas}@127.0.0.1:3306/{dbn}').format (
        nam=SQL_USER,
        pas=SQL_PASSWORD,
        dbn=SQL_DATABASE,
    )

class TestingConfig(Config):
    SQLALCHEMY_DATABASE_URI = (
        'mysql+pymysql://{nam}:{pas}@/{dbn}?unix_socket=/cloudsql/{con}').format (
        nam=SQL_USER,
        pas=SQL_PASSWORD,
        dbn=SQL_DATABASE,
        con=SQL_CONNECTION_NAME,
    )

class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = (
        'mysql+pymysql://{nam}:{pas}@localhost/{dbn}?unix_socket=/cloudsql/{con}').format (
        nam=SQL_USER,
        pas=SQL_PASSWORD,
        dbn=SQL_DATABASE,
        con=SQL_CONNECTION_NAME,
    )
    DEBUG = False
    TESTING = False
    SQLALCHEMY_ECHO = False


app_config = {
    "development": DevelopmentConfig,
    "testing": TestingConfig,
    "production": ProductionConfig
}
