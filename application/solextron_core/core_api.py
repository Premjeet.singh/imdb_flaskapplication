from http import HTTPStatus
from flask import Flask, Response, json


class FlaskAPIResponse(Flask):
    ##Make the base response
    def make_response(self, rv):
        if isinstance(rv, SuccessAPIResponse):
            return rv.to_json()
        if isinstance(rv, FailureAPIResponse):
            return rv.to_json()
        return super(FlaskAPIResponse, self).make_response(rv)


class SuccessAPIResponse:
    def __init__(self, message="Success", data=None, status=HTTPStatus.OK):
        self.data = {
            "status": True,
            "data": data if data else {},
            "message": message,
            "type": "success",
            "httpStatusCode": status,
        }
        self.status = status

    def to_json(self):
        return Response(
            json.dumps(self.data), 
            status=self.status, 
            mimetype="application/json"
        )


class FailureAPIResponse(Exception):
    def __init__(self, message="Error", data=None, status=HTTPStatus.INTERNAL_SERVER_ERROR):
        self.data = {
            "status": False,
            "data": data if data else {},
            "message": message,
            "type": "fail",
            "httpStatusCode": status,
        }
        self.status = status

    def to_json(self):
        return Response(
            json.dumps(self.data), 
            status=self.status, 
            mimetype="application/json"
        )
