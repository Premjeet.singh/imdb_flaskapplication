from sqlalchemy import desc, asc
from http import HTTPStatus
from datetime import datetime
from application.solextron_core.core_api import SuccessAPIResponse, FailureAPIResponse
from application.models.IMDB.IMDb_movies import (
    Movies,
    MoviesSchema
)
from application.models.IMDB.IMDb_names import (
    MovieName,
    MovieNameSchema

)
from application.models.IMDB.IMDb_ratings import (
    MovieRating,
    MovieRatingSchema
)
from application.models.IMDB.IMDb_titleprincipals import (
    MovieTitle,
    MovieTitleSchema
)
from application import db


class MovieServices:
    def __init__(self):
        pass

    def list_movie(self, params):
        title = params.get("title")
        year = params.get("year")
        search = "%{}%".format(title)
        page = int(params.get("page", 1)) - 1
        count = int(params.get("count", 25))
        offset = page * count
        movie_schema = MoviesSchema(many=True)
        movies = None
        if title:
            if year:
                movies = Movies.query.order_by(
                    desc(Movies.date_published)).filter(
                    Movies.title.like(search)).filter(Movies.year == year).offset(offset).limit(count).all()
            else:
                movies = Movies.query.order_by(
                    desc(Movies.date_published)).filter(
                    Movies.title.like(search)).offset(offset).limit(count).all()

        else:
            movies = Movies.query.order_by(
                desc(Movies.date_published)).offset(offset).limit(count).all()

        res_data = movie_schema.dump(movies)
        ret_data = {"movies": res_data}
        return SuccessAPIResponse(
            message="Success",
            data=ret_data,
            status=HTTPStatus.OK
        )

    def get_movie(self, movie_id):
        movie_schema = MoviesSchema(many=True)
        movie = Movies.query.filter(Movies.imdb_db_title_id == movie_id).all()
        if not movie:
            return FailureAPIResponse(
                message="movie not found with given_id",
                data={},
                status=HTTPStatus.NOT_FOUND
            )

        res_data = movie_schema.dump(movie)
        return SuccessAPIResponse(
            message="Success",
            data=res_data,
            status=HTTPStatus.OK
        )


    def get_movie_rating(self, movie_id):
        movie = Movies.query.filter(Movies.imdb_db_title_id == movie_id).all()
        if not movie:
            return FailureAPIResponse(
                message="movie not found",
                data={},
                status=HTTPStatus.NOT_FOUND
            )

        rating_schema = MovieRatingSchema(many=True)
        movie_ratings = MovieRating.query.filter(MovieRating.imdb_db_title_id == movie_id).all()
        res_data = rating_schema.dump(movie_ratings)
        return SuccessAPIResponse(
                message="Success",
                data=res_data,
                status = HTTPStatus.OK
        )


    def get_movie_cast(self, movie_id):
        movie = Movies.query.filter(MovieRating.imdb_db_title_id == movie_id).all()
        if not movie:
            return FailureAPIResponse(
                message="movie not found",
                data={},
                status=HTTPStatus.NOT_FOUND
            )
        title_schema = MovieTitleSchema(many=True)
        movie_cast_details = MovieTitle.query.filter(
            MovieTitle.imdb_db_title_id == movie_id).all()

        res_data = title_schema.dump(movie_cast_details)
        return SuccessAPIResponse(
            message="Success",
            data=res_data,
            status=HTTPStatus.OK
        )

