from flask import Blueprint, request, json
from flask.views import MethodView
from http import HTTPStatus
from application.solextron_core.core_api import SuccessAPIResponse, FailureAPIResponse
from application.services.IMDB.movie_service import MovieServices


movie_bp = Blueprint('api/v1/movie', __name__)


class MovieAPIView(MethodView):
    def get(self):
        movie_services = MovieServices()
        params = request.args.to_dict()
        ret = movie_services.list_movie(params)
        return ret


class GetMovieAPIView(MethodView):
    def get(self, movie_id):
        movie_services = MovieServices()
        ret = movie_services.get_movie(movie_id)
        return ret
        

class GetMovieRatingAPIView(MethodView):
    def get(self, movie_id):
        movie_services = MovieServices()
        ret = movie_services.get_movie_rating(movie_id)
        return ret


class GetMovieCastAPIView(MethodView):
    def get(self, movie_id):
        movie_services = MovieServices()
        ret = movie_services.get_movie_cast(movie_id)
        return ret


movie_api_view = MovieAPIView.as_view('movie_api_view')
movie_bp.add_url_rule(
    '/api/v1/movie',
    view_func=movie_api_view,
    methods=['GET']
)

get_movie_api_view = GetMovieAPIView.as_view('get_movie_api_view')
movie_bp.add_url_rule(
    '/api/v1/movie/<string:movie_id>',
    view_func=get_movie_api_view,
    methods=['GET']
)

get_movie_rating = GetMovieRatingAPIView.as_view('get_project_last_chat_phase')
movie_bp.add_url_rule(
    '/api/v1/movie/<string:movie_id>/rating',
    view_func=get_movie_rating,
    methods=['GET']
)

get_movie_cast = GetMovieCastAPIView.as_view('get_movie_last_cast')
movie_bp.add_url_rule(
    '/api/v1/movie/<string:movie_id>/cast',
    view_func=get_movie_cast,
    methods=['GET']
)

