from application.solextron_core.core_api import FlaskAPIResponse
from flask_cors import CORS
from application.config.config import app_config
from application.instance.config import RestrictedAppConfig as restricted_config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

db = SQLAlchemy()
ma = Marshmallow()

def create_app(test_config=None):
    app = FlaskAPIResponse(__name__, instance_relative_config=True)
    CORS(app, resources={r"/api/*": {"origins": "*"}})
    app.config['CORS_HEADERS'] = 'Content-Type'
    app.config.from_object(app_config["development"])
    app.config.from_object(restricted_config)
    init_sentry()

    from application.models.IMDB.IMDb_movies import Movies
    from application.models.IMDB.IMDb_names import MovieName
    from application.models.IMDB.IMDb_ratings import MovieRating
    from application.models.IMDB.IMDb_titleprincipals import MovieTitle

    db.init_app(app)
    Migrate(app, db, compare_type=True)
    with app.app_context():
        from application.routes.IMDB.movie_view import movie_bp
        app.register_blueprint(movie_bp)

    return app


def init_sentry():
    sentry_sdk.init(
        dsn="https://64c20896cf204891a11a8667f923ede0@o455706.ingest.sentry.io/5463453",
        integrations=[FlaskIntegration()],
        traces_sample_rate=1.0
    )
    return

