from enum import Enum
from datetime import datetime
from application import db, ma
from sqlalchemy import asc
from marshmallow import Schema, fields


class MovieTitle(db.Model):
    __tablename__ = 'imdb_db_title_principals'
    imdb_db_title_id = db.Column(db.String(200), primary_key=True, autoincrement=True)
    ordering = db.Column(db.Integer, unique=True, nullable=False)
    imdb_db_name_id = db.Column(db.String(200))
    category = db.Column(db.TEXT, nullable=False)
    job = db.Column(db.TEXT, default=0)
    characters = db.Column(db.TEXT, default=0)

    def __repr__(self):
        return '<movie_title: {}>'.format(self.imdb_db_title_id)


class MovieTitleSchema(ma.Schema):
    imdb_title_id = fields.String()
    ordering = fields.Integer()
    category = fields.String()
    imdb_db_name_id=fields.String()
    job = fields.String()
    characters = fields.String()