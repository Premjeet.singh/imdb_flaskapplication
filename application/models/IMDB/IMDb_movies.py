from enum import Enum
from datetime import datetime
from application import db, ma
from sqlalchemy import asc
from marshmallow import Schema, fields
from application.models.IMDB.IMDb_titleprincipals import (
    MovieTitle
)
from application.models.IMDB.IMDb_ratings import (
    MovieRating
)


class Movies(db.Model):
    __tablename__ = 'imdb_db_movies'
    imdb_db_title_id = db.Column(db.String(200), primary_key=True, autoincrement=True)
    title = db.Column(db.String(200), unique=True, nullable=False)
    original_title = db.Column(db.String(200), default=0.0)
    year = db.Column(db.Integer)
    date_published = db.Column(db.DateTime(),default=datetime.utcnow)
    genre = db.Column(db.String(200), default=0)
    duration = db.Column(db.Integer(), default=0)
    country = db.Column(db.Text())
    language = db.Column(db.String(200))
    director = db.Column(db.String(200))
    writer = db.Column(db.String(200))
    production_company = db.Column(db.String(200))
    actors = db.Column(db.Text())
    description = db.Column(db.Text(), nullable=True)
    avg_vote = db.Column(db.Integer(), default=False)
    votes = db.Column(db.Integer(), default=False)
    budget = db.Column(db.Integer(), default=False)
    reviews_from_users = db.Column(db.Integer(), default=False)
    reviews_from_critics = db.Column(db.Integer(), default=False)

    def __repr__(self):
        return '<movies: {}>'.format(self.imdb_db_title_id)


class MoviesSchema(ma.Schema):
    imdb_title_id = fields.String()
    title = fields.String()
    original_title = fields.String()
    year = fields.Integer()
    date_published = fields.Integer()
    genre = fields.String()
    duration = fields.Integer()
    country = fields.String()
    language = fields.String()
    director = fields.String()
    writer = fields.String()
    production_company = fields.String()
    actors = fields.String()
    description = fields.String()
    avg_vote = fields.Integer()
    votes = fields.Integer()
    budget = fields.Integer()
    reviews_from_users = fields.Integer()
    reviews_from_critics = fields.Integer()
