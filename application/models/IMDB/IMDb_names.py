from enum import Enum
from datetime import datetime
from application import db, ma
from sqlalchemy import asc
from marshmallow import Schema, fields
from application.models.IMDB.IMDb_titleprincipals import (
    MovieTitle
)


class MovieName(db.Model):
    __tablename__ = 'imdb_db_names'
    imdb_db_name_id = db.Column(db.String(200), primary_key=True, autoincrement=True)
    name = db.Column(db.String(200), unique=True, nullable=True)
    birth_name = db.Column(db.String(200), default=0.0)
    height = db.Column(db.DATE, nullable=True)
    bio = db.Column(db.TEXT, default=0)
    birth_details = db.Column(db.TEXT, default=0)
    date_of_birth = db.Column(db.DATE)
    place_of_birth = db.Column(db.TEXT)
    date_of_death = db.Column(db.DATE)
    place_of_death = db.Column(db.TEXT)
    reason_of_death = db.Column(db.VARCHAR(255),nullable=True)
    spouses_string = db.Column(db.Text(), nullable=True)
    spouses = db.Column(db.Integer(), default=0)
    divorces = db.Column(db.Integer(), default=0)
    spouses_with_children = db.Column(db.Integer(), default=0)
    children = db.Column(db.Integer(), default=0)
    reviews_from_users = db.Column(db.Integer(), default=0)
    reviews_from_critics = db.Column(db.Integer(), default=0)

    def __repr__(self):
        return '<movie_names: {}>'.format(self.imdb_db__name_id)


class MovieNameSchema(ma.Schema):
    imdb_title_id = fields.String()
    name = fields.String()
    birth_name = fields.String()
    height = fields.Integer()
    bio = fields.String()
    birth_details = fields.Integer()
    date_of_birth = fields.Date()
    place_of_birth = fields.String()
    date_of_death = fields.Date()
    place_of_death = fields.String()
    reason_of_death = fields.String()
    spouses_string = fields.String()
    spouses = fields.Integer()
    divorces = fields.Integer()
    spouses_with_children = fields.Integer()
    children = fields.Integer()
    reviews_from_users = fields.Integer()
    reviews_from_critics = fields.Integer()



