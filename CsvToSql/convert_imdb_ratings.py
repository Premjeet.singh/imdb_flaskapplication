import csv
import MySQLdb

# # Open database connection
db = MySQLdb.connect(
"localhost", ## Host
"root", ## Username
"root", ## Pass
"imdb_movies" ## DB
)

# prepare a cursor object using cursor() method
cursor = db.cursor()

# Create Table
cursor.execute("CREATE TABLE If Not EXISTS `imdb_ratings` ( `imdb_title_id` text, `weighted_average_vote` double DEFAULT NULL, `total_votes` int DEFAULT NULL, `mean_vote` double DEFAULT NULL, `median_vote` double DEFAULT NULL, `votes_10` int DEFAULT NULL, `votes_9` int DEFAULT NULL, `votes_8` int DEFAULT NULL, `votes_7` int DEFAULT NULL, `votes_6` int DEFAULT NULL, `votes_5` int DEFAULT NULL, `votes_4` int DEFAULT NULL, `votes_3` int DEFAULT NULL, `votes_2` int DEFAULT NULL, `votes_1` int DEFAULT NULL, `allgenders_0age_avg_vote` text, `allgenders_0age_votes` text, `allgenders_18age_avg_vote` double DEFAULT NULL, `allgenders_18age_votes` double DEFAULT NULL, `allgenders_30age_avg_vote` double DEFAULT NULL, `allgenders_30age_votes` double DEFAULT NULL, `allgenders_45age_avg_vote` double DEFAULT NULL, `allgenders_45age_votes` double DEFAULT NULL, `males_allages_avg_vote` double DEFAULT NULL, `males_allages_votes` double DEFAULT NULL, `males_0age_avg_vote` text, `males_0age_votes` text, `males_18age_avg_vote` double DEFAULT NULL, `males_18age_votes` double DEFAULT NULL, `males_30age_avg_vote` double DEFAULT NULL, `males_30age_votes` double DEFAULT NULL, `males_45age_avg_vote` double DEFAULT NULL, `males_45age_votes` double DEFAULT NULL, `females_allages_avg_vote` double DEFAULT NULL, `females_allages_votes` double DEFAULT NULL, `females_0age_avg_vote` text, `females_0age_votes` text, `females_18age_avg_vote` double DEFAULT NULL, `females_18age_votes` double DEFAULT NULL, `females_30age_avg_vote` double DEFAULT NULL, `females_30age_votes` double DEFAULT NULL, `females_45age_avg_vote` double DEFAULT NULL, `females_45age_votes` double DEFAULT NULL, `top1000_voters_rating` double DEFAULT NULL, `top1000_voters_votes` double DEFAULT NULL, `us_voters_rating` double DEFAULT NULL, `us_voters_votes` double DEFAULT NULL, `non_us_voters_rating` double DEFAULT NULL, `non_us_voters_votes` double DEFAULT NULL ) ;")

# Load CSV FILE
file = "C:\\Users\\dell\Desktop\\dataset\\IMDb ratings.csv"
csv_data = csv.reader(open(file,encoding='utf-8'))

count = 0
errorCount = 0
for row in csv_data:
    try:
        if count==0:
            pass
        else:
            cursor.execute("Insert Into imdb_ratings (imdb_title_id,  weighted_average_vote,  total_votes,  mean_vote,  median_vote,  votes_10,  votes_9,  votes_8,  votes_7,  votes_6,  votes_5,  votes_4,  votes_3,  votes_2,  votes_1,  allgenders_0age_avg_vote,  allgenders_0age_votes,  allgenders_18age_avg_vote,  allgenders_18age_votes,  allgenders_30age_avg_vote,  allgenders_30age_votes,  allgenders_45age_avg_vote,  allgenders_45age_votes,  males_allages_avg_vote,  males_allages_votes,  males_0age_avg_vote,  males_0age_votes,  males_18age_avg_vote,  males_18age_votes,  males_30age_avg_vote,  males_30age_votes,  males_45age_avg_vote,  males_45age_votes,  females_allages_avg_vote,  females_allages_votes,  females_0age_avg_vote,  females_0age_votes,  females_18age_avg_vote,  females_18age_votes,  females_30age_avg_vote,  females_30age_votes,  females_45age_avg_vote,  females_45age_votes,  top1000_voters_rating,  top1000_voters_votes,  us_voters_rating,  us_voters_votes,  non_us_voters_rating,  non_us_voters_votes) Values (%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s,%s ,%s ,%s ,%s ,%s ,%s ,%s, %s);",row)
        if count==100000:
            break
    except Exception as e:

        errorCount += 1
    count +=1


print(errorCount)

db.commit()
db.close()
