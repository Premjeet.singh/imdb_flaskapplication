import csv
import MySQLdb


# # Open database connection
db = MySQLdb.connect(
"localhost", ## Host
"root", ## Username
"root", ## Pass
"imdb_movies" ## DB
)

# prepare a cursor object using cursor() method
cursor = db.cursor()

# Create Table
cursor.execute("CREATE TABLE IF NOT EXISTS `imdb_title_principals` ( `imdb_title_id` text,`ordering` int DEFAULT NULL,  `imdb_name_id` text,  `category` text,  `job` text,  `characters` text); ")

# Load CSV Data
csv_data = csv.reader(open("C:\\Users\\dell\\Desktop\dataset\\IMDb title_principals.csv",encoding='utf-8'))

count = 0
for row in csv_data:
    if count == 0:
        pass
    else:
        cursor.execute("INSERT INTO `imdb_title_principals` ( `imdb_title_id`,`ordering`,  `imdb_name_id`,  `category` ,  `job` ,  `characters`) VALUES (%s ,%s ,%s ,%s ,%s ,%s); ",row)
    if count == 100000:
        break

    count += 1

db.commit()
db.close()

