import csv
import MySQLdb
from datetime import datetime


# # Open database connection
db = MySQLdb.connect(
"localhost", ## Host
"root", ## Username
"root", ## Pass
"imdb_movies" ## DB
)

# prepare a cursor object using cursor() method
cursor = db.cursor()

# Create Table
cursor.execute("CREATE TABLE If Not EXISTS `imdb_movies` ( `imdb_title_id` text, `title` text, `original_title` text, `year` int DEFAULT NULL, `date_published` datetime, `genre` text, `duration` int DEFAULT NULL, `country` text, `language` text, `director` text, `writer` text, `production_company` text, `actors` text, `description` text, `avg_vote` double DEFAULT NULL, `votes` int DEFAULT NULL, `budget` text, `usa_gross_income` text, `worlwide_gross_income` text, `metascore` text, `reviews_from_users` double DEFAULT NULL, `reviews_from_critics` double DEFAULT NULL );")

# Load CSV FILE
file = "C:\\Users\\dell\\Desktop\\dataset\\IMDb movies.csv"
csv_data = csv.reader(open(file,encoding='utf-8'))

count = 0
errorCount = 0
for row in csv_data:
    if count == 0:
        pass
    else:
        try:
            date_time_lst = row[4].split('-')
            row[4] = datetime(int(date_time_lst[0]),int(date_time_lst[1]),int(date_time_lst[2])).isoformat()
            cursor.execute("Insert Into imdb_movies (imdb_title_id,  title,  original_title,  year,  date_published,  genre,  duration,  country,  language,  director,  writer,  production_company,  actors,  description,  avg_vote,  votes,  budget,  usa_gross_income,  worlwide_gross_income,  metascore,  reviews_from_users,  reviews_from_critics) Values (%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s);",row)
            print("Data Inserted : ",row)
        except Exception as e:
            print("Error XXXX : "+str(e),row)
            errorCount +=1

    if count == 100000:
        break

    count += 1


print(errorCount)

db.commit()
db.close()
