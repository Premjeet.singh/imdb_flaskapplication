import csv
import MySQLdb

# # Open database connection
db = MySQLdb.connect(
"localhost", ## Host
"root", ## Username
"root", ## Pass
"imdb_movies" ## DB
)

# prepare a cursor object using cursor() method
cursor = db.cursor()

# Create Table
cursor.execute("Create Table IF NOT EXISTS imdb_names (`imdb_name_id` text, `name` text, `birth_name` text, `height` text, `bio` text, `birth_details` text, `date_of_birth` text, `place_of_birth` text, `death_details` text, `date_of_death` text, `place_of_death` text, `reason_of_death` text, `spouses_string` text, `spouses` text, `divorces` text, `spouses_with_children` text, `children` text);")

# Load CSV FILE
file = r"C:\Users\dell\Downloads\IMDb names.csv"
csv_data = csv.reader(open(file,encoding='utf-8'))

count = 0

for row in csv_data:
    if count==0:
        pass
    else:
        try:
            cursor.execute("Insert Into imdb_names (imdb_name_id,  name,  birth_name,  height,  bio,  birth_details,  date_of_birth,  place_of_birth,  death_details,  date_of_death,  place_of_death,  reason_of_death,  spouses_string,  spouses,  divorces,  spouses_with_children,  children) Values (%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s ,%s,%s,%s ,%s);",row)
        except Exception as e:
            print(e)
    if count==100000:
        break

    count +=1

db.commit()
db.close()
