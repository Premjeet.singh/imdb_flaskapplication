from application import create_app
from application.solextron_core.core_api import SuccessAPIResponse
app = create_app()


@app.route('/')
def hello():
    """Return a friendly HTTP greeting."""
    return "It's working fine"


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)