# IMDB MOVIE DATASET (Assignment)
IMDb is the most popular movie website and it combines movie plot description, Metastore ratings, critic and user ratings and reviews, release dates, and many more aspects.
## Getting Started
1. Make sure to have installed python3 on your local system
2. Install pip using --> sudo apt-get install python3-pip
3. install virtual environment using --> sudo pip3 install virtualenv 
4. create virtual environment --> virtualenv imdb_venv
5. Activate the above created virtualenv --> source imdb_venv/bin/activate
6. install requirements.txt using --> pip install -r requirements.txt
7. Make sure to have mysql installed and created database
8. Update the database configuration in the development configuration
9. Migrate the model changes to database --> Flask db upgrade
10. Run the development server using --> python app.py
11. Open the browser and make the request to url http://0.0.0.0:5001/

## Run the project

Run app.py file
